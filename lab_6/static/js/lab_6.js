$(document).ready(function(){
    // ChatBox
    $(".chat-text").on("keyup", function(e) {
        if (e.keyCode == 13 ){
            var str = $("textarea").val();
            $("textarea").val('');
            $(".msg-insert").append("<div class=\"msg-send\">" + str + "</div>");
            $(".msg-insert").append("<div class=\"msg-receive\">" + str + "!</div>");
        }
    })

    var themes = [
        {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
        {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
        {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
        {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
        {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
        {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
        {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
        {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
        {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
        {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
        {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ]

    var selectedTheme = {"text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"}

    if(typeof(Storage) !== "undefined") {
        localStorage.themes = JSON.stringify(themes);
        localStorage.selectedTheme = JSON.stringify(selectedTheme);
        changeColor(localStorage.selectedTheme);
    }


    $('.my-select').select2({
        'data': JSON.parse(localStorage.themes)
    })

    $('.apply-button').on('click', function(){
        console.log($(".my-select").select2("data"));
        var bgColor = $(".my-select").select2("data")[0]["bcgColor"];
        var fontColor = $(".my-select").select2("data")[0]["fontColor"];
        var temp = JSON.stringify({"bcgColor" : bgColor, "fontColor" : fontColor});
        localStorage.selectedTheme = temp;
        changeColor(temp);
    })

    function changeColor(data) {
        var obj = JSON.parse(data);
        $(".chat-head").css("background",obj["bcgColor"]);
        $("h2").css('color',obj['fontColor']);

        $(".calculator .model").css("background",obj["bcgColor"]);
        $("h1").css('color',obj['fontColor']);

    }

});

// Calculator
var go = function(x) {
  var print = document.getElementById("print");
  var erase = false;
  if (x === 'ac') {
      print.value = ''
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
      print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
