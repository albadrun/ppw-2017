from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from unittest import skip
from lab_2_addon.views import index, bio_dict

class Lab2AddonUnitTest(TestCase):

    def test_lab_2_addon_url_is_exist(self):
        response = Client().get('/lab-2-addon/')
        self.assertEqual(response.status_code, 200)

    def test_lab2_addon_using_index_func(self):
        found = resolve('/lab-2-addon/')
        self.assertEqual(found.func, index)

    def test_lab2_addon_bio_dict(self):
        self.assertIsNotNone(bio_dict)

        self.assertTrue(len(bio_dict) >= 3)

        for bio in bio_dict:
            self.assertIsNotNone(bio['subject'])
            self.assertIsNotNone(bio['value'])

            self.assertIsNot(type(bio['value']), type(8))

    def test_lab2_addon_bio_shown_in_page(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        for bio in bio_dict:
            self.assertIn('<td class="subject">' + bio['subject'] + '</td>', html_response)
            self.assertIn('<td class="value">' + bio['value'] + '</td>', html_response)
