from django.shortcuts import render
import datetime

mhs_name = 'Ragil Al Badrun Pasaribu'
birth_date = datetime.date(year=1998, month=6, day=28)

def index(request):
    response = {
        'name': mhs_name,
        'age': calculate_age(birth_date.year)
    }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return datetime.date.today().year - birth_year
