from django.shortcuts import render, redirect
from .models import Diary
import datetime
import json
import pytz

def index(request):
    queryset = Diary.objects.values()
    diary_dict = convert_queryset_into_json(queryset)
    response = {'diary_dict': diary_dict}
    return render(request, 'to_do_list.html', response)

def add_activity(request):
    if request.method == 'POST':
        activity = request.POST.get('activity')
        raw_date = request.POST.get('date')

        try:
            date = datetime.datetime.strptime(raw_date, '%Y-%m-%dT%H:%M')
            date = date.replace(tzinfo=pytz.UTC)
        except ValueError:
            return redirect('/lab-3/')

        Diary.objects.create(
            date=date,
            activity=activity
        )
        return redirect('/lab-3/')
    else:
        return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val